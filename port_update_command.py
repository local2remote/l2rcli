import requests
from config import Config
import json

class PortUpdateCommand:
    def __init__(self, port, shareToken, public, internal, private):
        self.port = port
        self.shareToken = shareToken
        self.public = public
        self.internal = internal
        self.private = private
        self.shareToken = shareToken
        self.config = Config()

    def run(self):
        data = self.getData(self.config.get('uid'), self.port)
        url = self.config.get('api_url')+'instance-ports'
        if data is None:
            print("Port not found")
            return
        access_level = int(data['access_level'])
        if self.public:
            access_level = 2
        if self.internal:
            access_level = 1
        if self.private:
            access_level = 0
        r = requests.put(url, json={
            "uid": self.config.get('uid'), 
            "port": self.port,
            "access_level": access_level,
            "shareToken": 1 if self.shareToken else 0,
            "name": data['name']
            },
            headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+self.config.get('token')
            })
        print(r.json()['message'])

    def getData(self, uid, port):
        url = self.config.get('api_url')+'instance-ports'
        r = requests.get(url, 
            headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+self.config.get('token')
            })
        for item in r.json():
            if int(item['port']) == int(port) and item['uid'] == uid:
                return item