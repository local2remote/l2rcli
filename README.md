# DriftStack CLI

## Commands

### Port List

Displays a list of open ports and their urls.

```
dscli port list
```

#### Options

```
  -a, --all
```

### Port Add Command

Opens a new port to the workspace.

```
dscli port add <port>
```

#### Options

```
  --public              Make public or not
  --share-token         Add share token
  -n NAME, --name NAME  Sub name for the domain
```

### Port Update Command

```
dscli port update <port>
```

#### Options

```
  --share-token  Add share token
```

### Port Delete Command

Removes an opend port.

```
dscli port delete <port>
```

### Bake Command

Runs predefined tasks.

```
dscli bake <path>
```

#### Options

```
  -a [KEY=VALUE [KEY=VALUE ...]], --answer [KEY=VALUE [KEY=VALUE ...]]
                        Add key/value params. May appear multiple times. Aggregate in dict
  --repo REPO           Recipe repo
```

### Credential Helper

Used for git credentials

```
dscli credential-helper
```