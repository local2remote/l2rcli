import configparser
from pathlib import Path
import os

class Config:
    def __init__(self):
        self.config = configparser.ConfigParser()
        configPath = os.path.join(Path.home(),".ds","config")
        if not os.path.exists(os.path.dirname(configPath)):
            os.mkdir(os.path.dirname(configPath))
        self.config.read(configPath)
    def get(self, key):
        return self.config['default'][key]