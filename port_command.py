import requests
from config import Config
import json
from port_update_command import PortUpdateCommand
from urllib.parse import urlparse


class PortCommand:
    def __init__(self, port, public, internal, name, shareToken=False):
        self.port = port
        self.public = public
        self.internal = internal
        self.config = Config()
        self.shareToken = shareToken
        self.name = name
        
    def run(self):
        access_level = 0
        if self.public:
            access_level = 2
        if self.internal:
            access_level = 1
        
        url = self.config.get('api_url')+'instance-ports'
        r = requests.post(url, json={
            "uid": self.config.get('uid'), 
            "port": self.port,
            "access_level": access_level,
            "name": self.name
            },
            headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+self.config.get('token')
            })
        data = r.json()
        self.message = data['message']
        self.url = data['data']['url']

        if self.shareToken:
            update = PortUpdateCommand(self.port, self.shareToken)
            update.run()
            self.url = update.url