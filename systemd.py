import os
import re
import subprocess

def create_service(command, name, working_dir=None, user='root'):
    # Sanitize the name parameter
    name = re.sub(r"[^a-zA-Z0-9_]+", "", name)
    service_file = f"/etc/systemd/system/{name}.service"
    if os.path.isfile(service_file):
        print(f"Error: {name} service already exists.")
        return

    # Elevate to root 
    subprocess.run(["sudo", "touch", service_file])
    # Elevate to root when writing to the service file
    if working_dir:
        subprocess.run(["sudo", "sh", "-c", f"echo '[Unit]\nDescription={name} service\n\n[Service]\nUser={user}\nExecStart={command}\nWorkingDirectory={working_dir}\n\n[Install]\nWantedBy=multi-user.target' > {service_file}"])
    else:
        subprocess.run(["sudo", "sh", "-c", f"echo '[Unit]\nDescription={name} service\n\n[Service]\nUser={user}\nExecStart={command}\n\n[Install]\nWantedBy=multi-user.target' > {service_file}"])

    os.system(f"sudo systemctl start {name}")
    os.system(f"sudo systemctl enable {name}")
    print(f"{name} service created and started successfully.")
